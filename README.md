#Teste Avenue Code desenvolvimento de WebService Restfull

Foram utilizados:
Tomcat8, Jersey, Hibernate, H2 e JUnit

A aplicação foi compilada e testada no jdk1.7.0_71

1. Para instalar a aplicação utilizar o comando 
 mvn clean install

2. para rodar o tomcat
 mvn tomcat7:run

3. Para rodar a suite de testes basta entrar com o comando :
 mvn test

4. As funcionalidades solicitadas foram geradas, porém não foram criados testes para todos os serviços gerados. 
Apesar de rodar no Tomcat localmente com sucesso existe uma falha na chamada "mvn Tomcat7:run"